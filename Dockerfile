#
#   Author: Rohith
#   Date: 2015-06-15 20:40:29 +0100 (Mon, 15 Jun 2015)
#
#  vim:ts=2:sw=2:et
#
FROM deis/store-admin:v1.8.0
MAINTAINER Rohith <gambol99@gmail.com>

ADD config/rbd /bin/rbd
ADD config/rbd-image /bin/rbd-image
ADD config/ceph /bin/ceph
ADD config/rados /bin/rados
ADD config/startup /bin/startup

ENV ETCD_HOST 127.0.0.1:4001

RUN chmod +x /bin/startup

CMD [ "/bin/startup" ]
