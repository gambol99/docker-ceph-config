#
#   Author: Rohith
#   Date: 2015-06-15 20:47:14 +0100 (Mon, 15 Jun 2015)
#
#  vim:ts=2:sw=2:et
#

AUTHOR=gambol99
NAME=ceph-config

.PHONY: build

default: build

build:
	sudo /usr/bin/docker build -t ${AUTHOR}/${NAME} .
